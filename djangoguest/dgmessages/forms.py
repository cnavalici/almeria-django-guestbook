# -*- coding: utf-8 -*-
from django import forms
from dgmessages.models import Message,Guest,Settings
from captcha.fields import CaptchaField

class AddNewForm(forms.Form):
    name        = forms.CharField(max_length=100)
    URL         = forms.URLField(required=False)
    email       = forms.EmailField(required=False)
    message     = forms.CharField(max_length=500,widget=forms.Textarea)
    captcha     = CaptchaField()

    def clean_name(self):
        name = self.cleaned_data['name'].strip()
        if not name:
            raise forms.ValidationError(u'Name cannot be empty. Please add a name.')
        return name


class SettingsForm(forms.Form):
    enable_postings = forms.BooleanField(required=False)
    posts_number    = forms.IntegerField(min_value = 3,max_value = 100,required=False,
                        label = "Posts/page (3-100)")
    site_title      = forms.CharField(max_length=50,required=False)
