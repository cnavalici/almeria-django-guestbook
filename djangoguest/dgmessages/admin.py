# -*- coding: utf-8 -*-
from djangoguest.dgmessages.models import Guest,Message
from django.contrib import admin

admin.site.register(Message)
admin.site.register(Guest)
