# -*- coding: utf-8 -*-
from django.db import models
from datetime import datetime

# guests
class Guest(models.Model):
    name = models.CharField("name", max_length=100)
    URL  = models.URLField(blank=True,null=True)
    email = models.EmailField(blank=True,null=True)

    def __unicode__(self):
        return "%s %s" % (self.name,self.URL)

# messages
class Message(models.Model):
    guest = models.ForeignKey(Guest)
    message = models.CharField(max_length=500)
    date = models.DateTimeField('date added')

    def __unicode__(self):
        return "%s -%s " % (self.guest,self.message)

    def save(self):
        self.date = datetime.now()
        super(Message, self).save()



# settings
class Settings(models.Model):
    name = models.CharField("name", max_length=100)
    value_bool = models.NullBooleanField(default=False)
    value_char = models.CharField(max_length=100,blank=True,null=True,default='')
    value_int  = models.IntegerField(blank=True,null=True,default=0)
    
    def __unicode__(self):
        return "%s" % (self.name)