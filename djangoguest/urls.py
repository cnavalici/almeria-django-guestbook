# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *
from django.conf import settings
from django.contrib.auth.views import login, logout
from dgmessages.views import *

# Uncomment the next two lines to enable the admin:
#from django.contrib import admin
#admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    # (r'^djangoguest/', include('djangoguest.foo.urls')),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    #(r'^admin/', include(admin.site.urls)),
    url(r'^$', index_page, name="main_page"),
    url(r'^add/$', addpost_page, name="add_post"),
    url(r'^captcha/', include('captcha.urls')),
    (r'^admin/login/$', login),
    url(r'^admin/logout/$', admin_logout_page, name="admin_logout"),
    url(r'^admin/$',requires_login(admin_index_page), name="admin_list"),
    url(r'^admin/settings/$',requires_login(admin_settings_page), name="admin_settings"),
    url(r'^admin/deletepost/(?P<mid>\w+)/$',requires_login(admin_delete_post),name="admin_delete_post"),
)

# we use this trick to prevent it slip into production environment
if settings.DEBUG:
    urlpatterns += patterns('django.views.static',
        (r'^media/(?P<path>.*)$', 
            'serve', {
            'document_root': settings.MEDIA_ROOT,
            'show_indexes': True })
         ,)
      